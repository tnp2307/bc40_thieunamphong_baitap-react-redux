const initialValue = {
  number: 10,
};

export const numberReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "TANG": {
      state.number++;
      return { ...state };
    }

    case "GIAM": {
      state.number--;
      return { ...state };
    }
    default:
      return state;
  }
};

