import React, { Component } from 'react'
import {connect} from "react-redux"

class ReduxEx extends Component {
  render() {
    return (
      <div className='container'>
        <button className='btn btn-danger' onClick={this.props.handleGiam}>
          -
        </button>
        <span>{this.props.count}</span>
        <button className='btn btn-success' onClick={this.props.handleTang}>+</button>
      </div>
    )
  }
}
let mapStateToProps =(state)=>{
  return {
    count: state.numberReducer.number
  }
}
let mapDispatchToProps = (dispatch)=>{
  return {
    handleTang:()=>{
      let action = {type: "TANG"}
      dispatch (action)
    },
    handleGiam :()=>{
      let action = {type: "GIAM"}
      dispatch(action)
    },
  }
}

export default connect (mapStateToProps,mapDispatchToProps)(ReduxEx)