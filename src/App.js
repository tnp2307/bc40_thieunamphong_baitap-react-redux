import './App.css';
import ReduxEx from './Redux-ex/ReduxEx';

function App() {
  return (
    <div>
      <ReduxEx/>
    </div>
  );
}

export default App;
